describe('Add and delete a to-do item', function() {
    beforeEach(module('app'));

    var $controller;

    beforeEach(inject(function(_$controller_){
        $controller = _$controller_;
    }));

    describe('$scope.items', function() {
        beforeEach(inject(function(_$controller_){
            var $scope = {};
            var controller = $controller('MainController', { $scope: $scope });
        }));
        it('adds an item to the list', function() {

            $scope.newItem = "Jasmine test item";
            $scope.adItem();
            $scope.originalLength = $scope.items.length;
            expect($scope.items.length).toEqual($scope.originalLength + 1);
        });
    });
});