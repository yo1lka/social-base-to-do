app.controller('MainController', function($scope, $firebaseArray, $timeout){

    //If I had another API, I would have done it through a service,
    //brining it reference into the controller
    //However, working with the service would require q bit more more coding
    //and working with IDs of the items in collection
    var fb = new Firebase("https://cloning-task.firebaseio.com");
    $scope.items="";
    $scope.items = $firebaseArray(fb);

    $scope.newItem = "";
    $scope.deletedItem = "";
    $scope.itemEdited = null;
    $scope.filter = 'all';
    $scope.itemsCount = 0;
    $scope.itemsCompleted = 0;
    $scope.itemsNotCompled = 0;

    $scope.modalVisible = false;


    function updateCounters(){
        console.log($scope.items);
        $scope.itemsCount = $scope.items.length;
        var counter = 0;
        $scope.items.forEach(function (item) {
            if (item.completed) {
                counter++
            }
        });
        $scope.itemsCompleted = counter;
        $scope.itemsNotCompled = $scope.itemsCount - $scope.itemsCompleted;
    }


    $scope.items.$loaded().then(updateCounters);

    //watch items in the list to recount the filter badges
    $scope.$watch('items', updateCounters);
    //regular $watch doesn't look throught the lengthm of the array
    //so we have to watch the lenght separately
    $scope.$watch('items.length', updateCounters);

    $scope.addItem = function(){
        if (!$scope.newItem) {
            return;
        }
        else {
            $scope.items.$add({
                title: $scope.newItem,
                completed: false,
                addDate: Firebase.ServerValue.TIMESTAMP,
                finishDate: null
            })
        }

        $scope.newItem = "";
    };

    $scope.updateCompleted = function(item){
        if (item.completed) {
            item.completeDate = Firebase.ServerValue.TIMESTAMP;
        }
        else {
            item.completeDate = null;
        }
        $scope.items.$save(item);
    }

    $scope.itemRemove = function (item) {
        $scope.deletedItem = item;
        $scope.items.$remove(item);
        //here we show the modal by triggering its visibility variable
        //when the modal closes, bootstrap close method resets this variable to false
        $scope.modalVisible = true;
    };

});