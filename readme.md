#Install
- npm install
- bower install

#run
- Run in any local http server

#Comments
The to-do list was created using the firebase.com platform which provides
a very easy to-use API for storing data.
If there was a normal personal API, the app would use angular services,
however now the firebase API is used directly in the controller.

The setup is not complete due to time limits, however, the final version could
inlude complete deployment scripts using Gulp and its plugins along with a
built-in http server.

The unit test is not completed due to the time limit as well
