app.directive('alertMsg', function () {
    return {
        templateUrl: 'directives/alert-msg-template.html',
        restrict: 'E',
        replace: true,
        scope: {visible:'=', itemToShow: "="},
        transclude: true,
        link: function postLink(scope, element, attrs) {

            $(element).modal({
                show: false
            });

            scope.$watch(function(){return scope.visible;}, function(value){
                if(value == true){
                    $(element).modal('show');
                }else{
                    $(element).modal('hide');
                }
            });

            scope.$watch(function(){return scope.itemToShow;}, function(value){
                scope.item = value;
            });

            $(element).on('shown.bs.modal', function(){
                scope.$apply(function(){
                    scope.$parent[attrs.visible] = true;
                });
            });

            $(element).on('hidden.bs.modal', function(){
                scope.$apply(function(){
                    scope.$parent[attrs.visible] = false;
                });
                scope.$apply(function(){
                    scope.$parent[attrs.itemToShow] = null;
                });
            });
        }
    }
});