app.filter('completedFilter', function(){
    return function(items, completion) {
        var result = [];
        angular.forEach(items, function (item) {
            if (completion === true) {
                if (item.completed) {
                    result.push(item);
                }
            }
            else if (completion === false) {
                if (!item.completed) {
                    result.push(item);
                }
            }
            else {
                result.push(item);
            }
        });
        return result;
    }
});